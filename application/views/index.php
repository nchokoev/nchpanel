<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
	<title></title>
	<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/reset.css"/>
	<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/style.css"/>
	<link rel="stylesheet" href="<?php echo $this->config->base_url(); ?>assets/css/font-awesome.min.css"/>
</head>
<body>

<header class="header">
	<div class="navbar"></div>
	<div class="header-center">
		<img src="<?php echo $this->config->base_url(); ?>assets/img/logo.png" alt="notify" class="logo"/>
		<span class="description">Responsive admin panel.</span>
		<div class="icons">
			<span class="signin"><a href="<?php echo $this->config->base_url(); ?>index.php/user/login"><em class="fa fa-sign-in fa-2x"> Enter</em></a></span>
		</div>
	</div>
</header>

<section class="property">
	<div class="center">
		<div class="editable box">
			<i class="fa fa-cogs fa-3x"></i>
			<h6>Code Igniter</h6>
			<p>Code Igniter powered.</p>
		</div>
		
		<div class="editable box">
			<i class="fa fa-eye fa-3x"></i>
			<h6>Custom</h6>
			<p>Fully customizable.</p>
		</div>
		
		<div class="editable box end">
			<i class="fa fa-shield fa-3x"></i>
			<h6>Styles</h6>
			<p>Custom styles.</p>
		</div>
		<div class="clear"></div>
	</div>
</section>

<section class="team">
	<div class="center">
		<p class="say">"nchPanel is CodeIgniter powered fast and flexible admin panel. "<br/>
		<span>www.nchPanel.com</span></p>
	</div>
</section>
	
<footer class="footer">
	<div class="footer-center">
		<nav class="footer-menu">
			<ul>
				<li><a href="#">Demo</a></li>
				<li><a href="#">Contact</a></li>
				<li><a href="#">Press</a></li>
				<li><a href="#">E-mail</a></li>
				<li><a href="#">Support</a></li>
				<li><a href="#">Privacy Policy</a></li>
			</ul>
		</nav>
	</div>

</footer>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
</body>
</html>