<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>nchPanel</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="nchPanel, admin panel, responsive, code igniter.">

    <!-- The styles -->
    <link id="bs-css" href="<?php echo $this->config->base_url(); ?>assets/css/bootstrap-cerulean.min.css" rel="stylesheet">
	<link href="<?php echo $this->config->base_url(); ?>assets/css/c3.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $this->config->base_url(); ?>assets/css/default.css" rel="stylesheet">
    <link href='<?php echo $this->config->base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='<?php echo $this->config->base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='<?php echo $this->config->base_url(); ?>assets/bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='<?php echo $this->config->base_url(); ?>assets/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='<?php echo $this->config->base_url(); ?>assets/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='<?php echo $this->config->base_url(); ?>assets/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='<?php echo $this->config->base_url(); ?>assets/css/jquery.noty.css' rel='stylesheet'>
    <link href='<?php echo $this->config->base_url(); ?>assets/css/noty_theme_default.css' rel='stylesheet'>
    <link href='<?php echo $this->config->base_url(); ?>assets/css/elfinder.min.css' rel='stylesheet'>
    <link href='<?php echo $this->config->base_url(); ?>assets/css/elfinder.theme.css' rel='stylesheet'>
    <link href='<?php echo $this->config->base_url(); ?>assets/css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='<?php echo $this->config->base_url(); ?>assets/css/uploadify.css' rel='stylesheet'>
    <link href='<?php echo $this->config->base_url(); ?>assets/css/animate.min.css' rel='stylesheet'>

    <!-- jQuery -->
    <script src="<?php echo $this->config->base_url(); ?>assets/bower_components/jquery/jquery.min.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The icon -->
    <link rel="shortcut icon" href="<?php echo $this->config->base_url(); ?>img/nchPanel.ico">

</head>

<body>
    <!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"> 
                <img alt="nchPanel Logo" src="<?php echo $this->config->base_url(); ?>assets/img/logo.png" class="hidden-xs"/>
                <span>nchPanel</span></a>

            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i>
                    <span class="hidden-sm hidden-xs"> <?php echo $this->session->userdata('username') ?> </span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo $this->config->site_url() ?>/user/logout">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->
        </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">
        <!-- left menu starts -->
        <div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">

                    </div>
                    <ul class="nav nav-pills nav-stacked main-menu">
                        <li><a class="ajax-link" href=<?php echo $this->config->site_url() ?>><i class="glyphicon glyphicon-home"></i><span> Dashboard</span></a></li>
						<?php if (strcmp($this->session->userdata('user_role'), 'admin') == 0) { ?>
                        <li class="nav-header">Admin</li>
                        <li><a class="ajax-link" href="<?php echo $this->config->site_url() ?>/user/users"><i class="glyphicon glyphicon-user"></i><span> Users</span></a></li>
                        <li><a class="ajax-link" href="#"><i class="glyphicon glyphicon-edit"></i><span> Admin1</span></a></li>
                        <li><a class="ajax-link" href="#"><i class="glyphicon glyphicon-list-alt"></i><span> Admin2</span></a></li>
						<?php } ?>
						<li class="nav-header">General</li>
                        <li><a class="ajax-link" href="<?php echo $this->config->site_url() ?>/user/computers"><i class="glyphicon glyphicon-phone"></i><span> Computers</span></a></li>
                        <li><a class="ajax-link" href="#"><i class="glyphicon glyphicon-search"></i><span> Search</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!--/span-->
        <!-- left menu ends -->

        <noscript>
            <div class="alert alert-block col-md-12">
                <h4 class="alert-heading">Warning!</h4>

                <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                    enabled to use this site.</p>
            </div>
        </noscript>

        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
