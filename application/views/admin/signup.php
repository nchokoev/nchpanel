<!DOCTYPE html> 
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>nchPanel Sign Up</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="nchPanel, admin panel, responsive, CodeIgniter.">

    <!-- The styles -->
    <link id="bs-css" href="<?php echo base_url(); ?>assets/css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets/css/default.css" rel="stylesheet">
    <link href='<?php echo base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='<?php echo base_url(); ?>assets/bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='<?php echo base_url(); ?>assets/bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='<?php echo base_url(); ?>assets/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='<?php echo base_url(); ?>assets/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='<?php echo base_url(); ?>assets/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='<?php echo base_url(); ?>assets/css/jquery.noty.css' rel='stylesheet'>
    <link href='<?php echo base_url(); ?>assets/css/noty_theme_default.css' rel='stylesheet'>
    <link href='<?php echo base_url(); ?>assets/css/elfinder.min.css' rel='stylesheet'>
    <link href='<?php echo base_url(); ?>assets/css/elfinder.theme.css' rel='stylesheet'>
    <link href='<?php echo base_url(); ?>assets/css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='<?php echo base_url(); ?>assets/css/uploadify.css' rel='stylesheet'>
    <link href='<?php echo base_url(); ?>assets/css/animate.min.css' rel='stylesheet'>

    <!-- jQuery -->
    <script src="bower_components/jquery/jquery.min.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The icon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/logo.png">
</head>
  <body>
    <div class="row">
        <div class="col-md-12 center login-header">
            <h2>Sign Up</h2>
        </div>
        <!--/span-->
    </div><!--/row-->
    <div class="row">
        <div class="well col-md-5 center login-box">
            <div class="alert alert-info">
                Create new account.
            </div>
				<?php
				echo validation_errors();
				$attributes = array('class' => 'form-horizontal', 'id' => 'signupform');
				echo form_open('user/signup', $attributes);
				?>
                <fieldset>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
						<?php
						$data = array(
									  'name'        => 'username',
									  'id'          => 'username',
									  'class'       => 'form-control',
									  'placeholder' => 'Username',
									);
						echo form_input($data);
						?>
                    </div>
                    <div class="clearfix"></div><br>

                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                        <?php
						$data = array(
									  'name'        => 'password',
									  'id'          => 'password',
									  'type'        => 'password',
									  'class'       => 'form-control',
									  'placeholder' => 'Password',
									);
						echo form_input($data);
						?>
                    </div>
					<div class="clearfix"></div><br>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope red"></i></span>
                        <?php
						$data = array(
									  'name'        => 'email',
									  'id'          => 'email',
									  'type'        => 'email',
									  'class'       => 'form-control',
									  'placeholder' => 'Email',
									);
						echo form_input($data);
						?>
                    </div>
					<div class="clearfix"></div>

                    <p class="center col-md-5">
					<?php
					echo form_submit('submit', 'Sign Up', 'class="btn btn-large btn-primary"');
					?>
                    </p>
                </fieldset>
			<?php
			echo form_close();
            //</form>
			?>
        </div>
        <!--/span-->
    </div><!--/row-->
  </body>
</html>    
    