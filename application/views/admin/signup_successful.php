<!DOCTYPE html> 
<html lang="en-US">
  <head>
    <title>nchPanel email confirmation.</title>
    <meta charset="utf-8">
    <link href="<?php echo base_url(); ?>assets/css/admin/global.css" rel="stylesheet" type="text/css">
  </head>
  <body>
  <div class="form-signin">
    <h1>Congrats!</h1>
	<p>An e-mail with a confirmation link has been sent to your e-mail account.</p>
  </div>
    <script src="<?php echo base_url(); ?>assets/js/jquery-1.7.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
  </body>
</html>    
    