<div>
    <ul class="breadcrumb">
        <li>
            <a>Computers</a>
        </li>
    </ul>
</div>

<?php for ($r = 0; $r < $rows; $r++) { ?>
<div class=" row">
	<?php for ($c = 0; $c < $cols; $c++) { ?>
    <div class="col-md-3 col-sm-3 col-xs-6">
        <a data-toggle="tooltip" class="well top-block" href=<?php echo $computer_link[$r*$cols+$c]?>>
            <i class="glyphicon glyphicon-phone blue"></i>
            <div><?php echo $computer_name[$r*$cols+$c] ?></div>
        </a>
    </div>
	<?php } ?>
</div>
<?php } ?>