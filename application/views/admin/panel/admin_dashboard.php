<div>
    <ul class="breadcrumb">
        <li>
            <a>Users</a>
        </li>
    </ul>
</div>
<div class=" row">
    <div class="col-md-3 col-sm-3 col-xs-6">
        <a data-toggle="tooltip" title="<?php echo $new_users ?> new users." class="well top-block" href="#">
            <i class="glyphicon glyphicon-user blue"></i>
            <div>Total Users</div>
            <div><?php echo $total_users ?></div>
			<?php if($new_users > 0 ) {?>
            <span class="notification"><?php echo $new_users ?> </span>
			<?php } ?>
        </a>
    </div>

    <div class="col-md-3 col-sm-3 col-xs-6">
        <a data-toggle="tooltip" class="well top-block" href="#">
            <i class="glyphicon glyphicon-star green"></i>
            <div>Admin Users</div>
            <div><?php echo $admin_users ?></div>
        </a>
    </div>

    <div class="col-md-3 col-sm-3 col-xs-6">
        <a data-toggle="tooltip" class="well top-block" href="#">
            <i class="glyphicon glyphicon-user red"></i>
            <div>Pending Users</div>
            <div><?php echo $pending_users ?></div>
        </a>
    </div>
</div>
