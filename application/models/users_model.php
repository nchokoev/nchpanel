<?php

class Users_model extends CI_Model {

    /**
    * get_user_data from the database
    * @param string $username
    * @param string $password
    * @return void
    */
	function get_user_data($username, $password)
	{
		$this->db->where('username', $username);
		$this->db->where('password', hash('sha256', $password));
		$query = $this->db->get('users');
		
		if($query->num_rows == 1)
		{
			$user_id = $query->row()->user_id;
			$user_data = array(
							'username' => $username,
							'user_role' => $this->get_user_role($user_id),
						);
			return $user_data;
		}
	}

	function get_user_role($user_id)
	{
		$this->db->where('user_id', $user_id);
		$query = $this->db->get('users');
		
		if($query->num_rows == 1)
		{
			$user_role_id = $query->row()->role_id;
			$this->db->where('role_id', $user_role_id);
			$query = $this->db->get('roles');
			if($query->num_rows == 1)
			{
				return $query->row()->role;
			}
		}
	}

	function get_users_for_role($role)
	{
		$this->db->where('role', $role);
		$query = $this->db->get('roles');
		if($query->num_rows() > 0)
		{
			$role_id = $query->row()->role_id;
			$this->db->where('role_id', $role_id);
			$query = $this->db->get('users');
			return $query->num_rows();
		}
		return 0;
	}
	
	function get_pending_users()
	{
		$query = $this->db->get('users_nc');
		return $query->num_rows();
	}
	
	function get_all_users()
	{
		//Get all users
		$total_users = $this->db->get('users')->num_rows();
		//Get Admins
		$admin_users = $this->get_users_for_role('admin');
		$pending_users = $this->get_pending_users();
		$users = array(
			'total_users' => $total_users,
			'admin_users' => $admin_users,
			'pending_users' => $pending_users,
			);
		return $users;
	}

	function get_computers_for_user($username)
	{
		$machine_data = array();
		$this->db->where('username', $username);
		$query = $this->db->get('users');
		if($query->num_rows == 1)
		{
			$user_id = $query->row()->user_id;
			$this->db->where('user_id', $user_id);
			$query = $this->db->get('machines');
			$idx = 0;
			foreach ($query->result() as $row)
			{
				$machine_data[$idx++] = array(
									'uid' => $user_id,
									'mid' => $row->machine_id,
									'name' => $row->name,
									);
			}
		}
		return $machine_data;
	}
	
	function get_apps_for_computer($mid, $uid)
	{
		$apps_data = array();
		$sql="SELECT apps.app_path, apps.app_name, apps.app_id, SEC_TO_TIME(sum(times.duration_sec)) FROM apps INNER JOIN times ON apps.app_id = times.app_id and times.user_id= $uid and times.machine_id = $mid group by apps.app_id order by sum(times.duration_sec) desc";
		$query = $this->db->query($sql);		
		$idx=0;
		foreach ($query->result_array() as $row)
		{
			$apps_data[$idx++] = array(
								'app_id' => $row['app_id'],
								'app_path' => $row['app_path'],
								'app_name' => $row['app_name'],
								'duration' => $row['SEC_TO_TIME(sum(times.duration_sec))'],
								);
		}
		return $apps_data;
	}

	function get_app_times($mid, $uid, $aid)
	{
		$times_data = array();
		$this->db->where('app_id', $aid);
		$this->db->where('machine_id', $mid);
		$this->db->where('user_id', $uid);
		$query = $this->db->get('apps');
		if($query->num_rows() == 1)
		{
			$times_data['app_name'] = $query->row()->app_name;
			$this->db->where('app_id', $aid);
			$this->db->where('machine_id', $mid);
			$this->db->where('user_id', $uid);
			$query = $this->db->get('times');
			$idx=0;
			foreach ($query->result() as $row)
			{
				$times_data['data'][$idx] = array(
									'id' => $row->time_id,
									'start_time' => $row->start_time,
									'duration_sec' => $row->duration_sec,
									'idle_time_sec' => $row->idle_time_sec,
									'app_title' => $row->app_title,
									);
				$idx++;
			}
			
		}
		return $times_data;
	}
	
	function delete_app_times($mid, $uid, $aid)
	{
		$this->db->where('app_id', $aid);
		$this->db->where('machine_id', $mid);
		$this->db->where('user_id', $uid);
		$query = $this->db->delete('times');
		$this->db->where('app_id', $aid);
		$this->db->where('machine_id', $mid);
		$this->db->where('user_id', $uid);
		$query = $this->db->delete('apps');
		return $query;
	}

    /**
    * Serialize the session data stored in the database, 
    * store it in a new array and return it to the controller 
    * @return array
    */
	function get_db_session_data()
	{
		$query = $this->db->select('user_data')->get('ci_sessions');
		$user = array(); /* array to store the user data we fetch */
		foreach ($query->result() as $row)
		{
		    $udata = unserialize($row->user_data);
		    /* put data in array using username as key */
		    $user['username'] = $udata['username']; 
		    $user['is_logged_in'] = $udata['is_logged_in'];
			$user['user_role'] = $udata['user_role'];
		}
		return $user;
	}
	
	function confirm_account($token)
	{
		$this->db->where('confirm_code', $token);
		$query = $this->db->get('users_nc');
        if($query->num_rows == 1)
		{
			$this->db->where('role', 'user');
			$roleres = $this->db->get('roles');
			if($roleres->num_rows == 1)
			{
				$new_account_data = array(
								'email' => $query->row()->email,			
								'username' => $query->row()->username,
								'password' => $query->row()->password,
								'role_id' => $roleres->row()->role_id
								);
				$insres = $this->db->insert('users', $new_account_data);
				if($insres)
				{
					$this->db->where('confirm_code', $token);
					$this->db->delete('users_nc');
					return $insres;
				}
			}
		}
	}
	
    /**
    * Store the new user's data into the database
    * @return boolean - check the insert
    */	
	function create_account()
	{
		$this->db->where('username', $this->input->post('username'));
		$query = $this->db->get('users');
        if($query->num_rows > 0){
        	echo '<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>';
			  echo "Username already taken";	
			echo '</strong></div>';
			return;
		}
		
		$this->db->where('email', $this->input->post('email'));
		$query = $this->db->get('users');
        if($query->num_rows > 0){
        	echo '<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>';
			  echo "User with this email address already exist.";	
			echo '</strong></div>';
			return;
		}

		$this->db->where('username', $this->input->post('username'));
		$query = $this->db->get('users_nc');
        if($query->num_rows > 0)
		{
        	echo '<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>';
			  echo "This account is pending email confirmation.";	
			echo '</strong></div>';
			return;
		}

		$this->db->where('email', $this->input->post('email'));
		$query = $this->db->get('users_nc');
        if($query->num_rows > 0){
        	echo '<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>';
			  echo "User with this email address is pending confirmation.";	
			echo '</strong></div>';
			return;
		}

		// Random confirmation code 
		$confirm_code=md5(uniqid(rand())); 
		$new_account_data = array(
			'email' => $this->input->post('email'),			
			'username' => $this->input->post('username'),
			'password' => hash('sha256', $this->input->post('password')),
			'confirm_code' => $confirm_code,
		);
		$insert = $this->db->insert('users_nc', $new_account_data);
		if($insert)
		{
			// ---------------- SEND MAIL FORM ----------------
			// send e-mail to ...
			$to=$this->input->post('email');

			// Confirmation email subject
			$subject="nowaTime Account Activation";

			// From
			$header="from: nowaTime no_reply@nowatime.com";

			// Confirmation message
			$message="Hello, \r\n";
			$message.="\r\n";
			$message.="Thank you for signing up for nowaTime. Please follow this link to activate your account. \r\n";
			$message.="\r\n";
			$message.=$this->config->site_url()."/user/confirm/token/".$confirm_code;
			$message.="\r\n";
			$message.="\r\n";
			$message.="Thank You,\r\n";
			$message.="The nowaTime Team\r\n";

			// send email
			return mail($to,$subject,$message,$header);
		} 
	}//create_account
}

