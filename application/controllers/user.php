<?php

class User extends CI_Controller {

    /**
    * Check if the user is logged in, if he's not, 
    * send him to the login page
    * @return void
    */	
	function index()
	{
		if($this->session->userdata('is_logged_in'))
		{
			//load the view header
			$this->load->view('includes/header');
			// Main View
			$this->load->view('admin/panel/main');
			// Load the view footer
			$this->load->view('includes/footer');
        }else{
        	$this->load->view('index');
        }
	}	

	/* */
	function login()
	{
		if($this->session->userdata('is_logged_in'))
		{
			//load the view header
			$this->load->view('includes/header');
			//Users /* 0 - no header and footer */
			$this->_users(0);
			//Computers
			$this->_computers(0, 'computer');
			// Load the view footer
			$this->load->view('includes/footer');
        }else{
        	$this->load->view('admin/login');
        }
	}

	/* Private method to load users view.
 	 *
	 */
	function _users($load)
	{
		if($load != 0)
		{
			//load the view header
			$this->load->view('includes/header');
		}

		if (strcmp($this->session->userdata('user_role'), 'admin') == 0)
		{
			$this->load->model('Users_model');
			$users = $this->Users_model->get_all_users();
			$data['total_users'] = $users['total_users'];
			$data['admin_users'] = $users['admin_users'];
			$data['pending_users'] = $users['pending_users'];
			$data['new_users'] = 0;
			// Load admin view
			$this->load->view('admin/panel/admin_dashboard', $data);
		}

		if($load != 0)
		{
			//load the view header
			$this->load->view('includes/footer');
		}
	}
	
	/* Private method to load computers view.
 	 *
	 */
	function _computers($load, $link)
	{
		if($load != 0)
		{
			//load the view header
			$this->load->view('includes/header');
		}

		$this->load->model('Users_model');
		$computers = $this->Users_model->get_computers_for_user($this->session->userdata('username'));
		
		$idx = 0;
		$max_items_per_row = 3;
		$computer_name = array();
		$computer_link = array();
		foreach ($computers as $computer)
		{
			$computer_name[$idx] = $computer['name'];
			$computer_link[$idx] = $this->config->site_url()."/user/".$link;
			$computer_link[$idx] .= "/uid/".$computer['uid'];
			$computer_link[$idx] .= "/mid/".$computer['mid'];
			$idx++;
		}

		// Load general user view
		$user_data['cols'] = $idx<$max_items_per_row?$idx:$max_items_per_row;
		$user_data['rows'] = $idx<$max_items_per_row?1:($idx+$max_items_per_row-1)/$max_items_per_row;
		
		$user_data['computer_name'] = $computer_name;
		$user_data['computer_link'] = $computer_link;
		$this->load->view('admin/panel/computers', $user_data);
		if($load != 0)
		{
			//load the view header
			$this->load->view('includes/footer');
		}
	}
	
	function users()
	{
		if($this->session->userdata('is_logged_in'))
		{
			$this->_users(1);
        }
		else
		{
        	$this->load->view('admin/login');
        }
	}

	/* Load compuers view with header and footer.
 	 *
	 */
	function computers()
	{
		if($this->session->userdata('is_logged_in'))
		{
			$this->_computers(1, 'computer');
        }
		else
		{
        	$this->load->view('admin/login');
        }
	}
	
	/* Load view for the apps in the chosen computer.
 	 *
	 */
	function computer()
	{
		if($this->session->userdata('is_logged_in'))
		{
			//load the view header
			$this->load->view('includes/header');
			
			echo "Computer View Goes Here!";
			
			// Load the view footer
			$this->load->view('includes/footer');
        }else{
        	$this->load->view('admin/login');
        }
	}
	
    /**
    * check the username and the password with the database
    * @return void
    */
	function validate_credentials()
	{
		$this->load->model('Users_model');

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$user_data = $this->Users_model->get_user_data($username, $password);
		
		if($user_data)
		{
			$data = array(
				'username' => $username,
				'is_logged_in' => true,
				'user_role' => $user_data['user_role']
			);
			$this->session->set_userdata($data);
			redirect('user/index');
		}
		else // incorrect username or password
		{
			$data['message_error'] = TRUE;
			$this->load->view('admin/login', $data);	
		}
	}	

    /**
    * The method just loads the signup view
    * @return void
    */
	function signup()
	{
		$this->load->library('form_validation');
		
		// field name, error message, validation rules
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
		$this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
		
		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('admin/signup');
		}
		else
		{			
			$this->load->model('Users_model');
			
			if($query = $this->Users_model->create_account())
			{
				$this->load->view('admin/signup_successful');			
			}
			else
			{
				$this->load->view('admin/signup');			
			}
		}
	}

    /**
    * Create new user and store it in the database
    * @return void
    */	
	function confirm()
	{
		$this->load->model('Users_model');
		$data = $this->uri->uri_to_assoc(3);
		$res = $this->Users_model->confirm_account($data['token']);
		if($res)
		{
			//TODO
			//Confirmed
			$this->load->view('admin/confirmation_successful');
			return;
		}
		//TODO
		//Doesn't exist
		echo "Wrong Confirmation code";
	}
	
	/**
    * Destroy the session, and logout the user.
    * @return void
    */		
	function logout()
	{
		$this->session->sess_destroy();
		redirect('user/index');
	}
}